# registration_service/urls.py

from django.urls import path
from . import views

urlpatterns = [
    path('register/patient/', views.register_patient, name='register_patient'),
    path('register/doctor/', views.register_doctor, name='register_doctor'),
    path('register/staff/', views.register_staff, name='register_staff'),
    # Add URLs for other registration types as needed
]
