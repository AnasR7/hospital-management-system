# registration_service/serializers.py

from rest_framework import serializers
from users.models import Patient, Doctor, Staff

class PatientSerializer(serializers.ModelSerializer):
    """
    Serializer for the Patient model.
    """
    class Meta:
        model = Patient
        fields = '__all__'  # Include all fields from the Patient model

class DoctorSerializer(serializers.ModelSerializer):
    """
    Serializer for the Doctor model.
    """
    class Meta:
        model = Doctor
        fields = '__all__'  # Include all fields from the Doctor model

class StaffSerializer(serializers.ModelSerializer):
    """
    Serializer for the Staff model.
    """
    class Meta:
        model = Staff
        fields = '__all__'  # Include all fields from the Staff model
