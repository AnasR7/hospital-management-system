# registration_service/views.py

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from users.models import Patient, Doctor, Staff
from .serializers import PatientSerializer, DoctorSerializer, StaffSerializer

# Create your views here.

@api_view(['POST'])
def register_patient(request):
    """
    API endpoint for patient registration.
    """
    if request.method == 'POST':
        serializer = PatientSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def register_doctor(request):
    """
    API endpoint for doctor registration.
    """
    if request.method == 'POST':
        serializer = DoctorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def register_staff(request):
    """
    API endpoint for staff registration.
    """
    if request.method == 'POST':
        serializer = StaffSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
